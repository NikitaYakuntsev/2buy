package com.nktykntsv.to_buy.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan("com.nktykntsv.to_buy.jpa.domain")
@EnableJpaRepositories(value = "com.nktykntsv.to_buy.jpa.repository")
@EnableTransactionManagement
public class MainConfig {
}
