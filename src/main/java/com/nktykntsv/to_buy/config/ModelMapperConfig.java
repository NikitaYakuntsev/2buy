package com.nktykntsv.to_buy.config;

import com.nktykntsv.to_buy.jpa.domain.Barcode;
import com.nktykntsv.to_buy.jpa.domain.Category;
import com.nktykntsv.to_buy.jpa.domain.Product;
import com.nktykntsv.to_buy.web.dto.BarcodeDto;
import com.nktykntsv.to_buy.web.dto.CategoryDto;
import com.nktykntsv.to_buy.web.dto.IdNameDto;
import com.nktykntsv.to_buy.web.dto.ProductDto;
import org.hibernate.collection.spi.PersistentCollection;
import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        setLazyCollectionsConverters(mapper);
        setDtoSpecificConverters(mapper);
        return mapper;
    }

    private void setLazyCollectionsConverters(ModelMapper mapper) {
        Condition<Object, Object> initializedCollection = context ->
                !(context.getSource() instanceof PersistentCollection) ||
                        ((PersistentCollection) context.getSource()).wasInitialized();
        mapper.getConfiguration().setPropertyCondition(initializedCollection);
    }

    private void setDtoSpecificConverters(ModelMapper mapper) {
        mapper.createTypeMap(Barcode.class, BarcodeDto.class).addMapping(Barcode::getCode, BarcodeDto::setName);
        mapper.createTypeMap(BarcodeDto.class, Barcode.class).addMapping(IdNameDto::getName, Barcode::setCode);
        mapper.createTypeMap(Category.class, CategoryDto.class).setPostConverter(context -> {
            Category parentCategory = context.getSource().getParentCategory();
            if (parentCategory != null) {
                IdNameDto parentCategoryDto = mapper.map(parentCategory, IdNameDto.class);
                context.getDestination().setParentCategory(parentCategoryDto);
            }
            return context.getDestination();
        });
        mapper.createTypeMap(Product.class, ProductDto.class).setPostConverter(context -> {
            context.getDestination().setCategory(mapper.map(context.getSource().getCategory(), CategoryDto.class));
            return context.getDestination();
        });
    }
}
