package com.nktykntsv.to_buy.service;

import com.nktykntsv.to_buy.exception.ItemNotFoundException;
import com.nktykntsv.to_buy.jpa.domain.Barcode;
import com.nktykntsv.to_buy.jpa.domain.Product;
import com.nktykntsv.to_buy.jpa.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ManufacturerService manufacturerService;
    private final CategoryService categoryService;

    @Autowired
    public ProductService(ProductRepository productRepository, ManufacturerService manufacturerService, CategoryService categoryService) {
        this.productRepository = productRepository;
        this.manufacturerService = manufacturerService;
        this.categoryService = categoryService;
    }

    public Product findProductByBarcode(String barcode) {
        return productRepository.findTopByBarcode_Code(barcode).orElse(null);
    }

    public Product findProductById(Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(String.format("Product with ID [%d] was not found", id)));
    }

    @Transactional
    public Product resolveProduct(Product product) {
        if (product.getId() != null) {
            Optional<Product> productOptional = productRepository.findById(product.getId());
            if (productOptional.isPresent()) {
                return productOptional.get();
            }
            product.setId(null);
        }
        Optional<Product> productWithBarcode = Optional.ofNullable(product.getBarcode())
                .map(Barcode::getCode)
                .map(this::findProductByBarcode);
        if (productWithBarcode.isPresent()) {
            return productWithBarcode.get();
        }
        product.setManufacturer(manufacturerService.resolveManufacturer(product.getManufacturer()));
        product.setCategory(categoryService.resolveCategory(product.getCategory()));
        return productRepository.save(product);
    }
}
