package com.nktykntsv.to_buy.service;

import com.nktykntsv.to_buy.jpa.domain.Manufacturer;
import com.nktykntsv.to_buy.jpa.repository.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ManufacturerService {

    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerService(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Transactional
    public Manufacturer resolveManufacturer(Manufacturer manufacturer) {
        if (manufacturer.getId() != null) {
            Optional<Manufacturer> manufacturerOptional = manufacturerRepository.findById(manufacturer.getId());
            if (manufacturerOptional.isPresent()) {
                return manufacturerOptional.get();
            }
            manufacturer.setId(null);
        }
        return manufacturerRepository.save(manufacturer);
    }
}
