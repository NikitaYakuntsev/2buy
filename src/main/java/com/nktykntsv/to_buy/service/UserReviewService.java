package com.nktykntsv.to_buy.service;

import com.nktykntsv.to_buy.jpa.domain.Product;
import com.nktykntsv.to_buy.jpa.domain.QUserReview;
import com.nktykntsv.to_buy.jpa.domain.User;
import com.nktykntsv.to_buy.jpa.domain.UserReview;
import com.nktykntsv.to_buy.jpa.repository.UserReviewRepository;
import com.nktykntsv.to_buy.web.dto.UserReviewFilterDto;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@Service
public class UserReviewService {

    private final UserReviewRepository userReviewRepository;
    private final ProductService productService;

    @Autowired
    public UserReviewService(UserReviewRepository userReviewRepository, ProductService productService) {
        this.userReviewRepository = userReviewRepository;
        this.productService = productService;
    }

    @Transactional
    public UserReview addUserReview(User user, Product product, Integer rating, String comment, Boolean isFavourite) {

        UserReview review = new UserReview();
        review.setUser(user);
        review.setProduct(productService.resolveProduct(product));
        review.setRating(rating);
        review.setComment(comment);
        review.setFavourite(isFavourite);
        review.setActive(true);
        review.setDeleted(false);
        review.setReviewDate(ZonedDateTime.now());

        userReviewRepository.deactivatePreviousReviews(user.getId(), review.getProduct().getId());

        return userReviewRepository.save(review);
    }

    public Page<UserReview> getUserReviews(User currentUser, UserReviewFilterDto filter, Pageable pageable) {
        Predicate predicate = prepareBaseCondition(currentUser)
                .and(prepareFilterPredicate(filter));

        QPageRequest qPageable = QPageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), new QSort(QUserReview.userReview.reviewDate.desc()));
        return userReviewRepository.findAll(predicate, qPageable);
    }

    public UserReview getUserReviewByProduct(User currentUser, Product product) {
        BooleanBuilder predicate = prepareBaseCondition(currentUser)
                .and(QUserReview.userReview.product.id.eq(product.getId()));
        return userReviewRepository.findOne(predicate).orElse(null);
    }

    public UserReview getUserReviewByBarcode(User currentUser, String barcode) {
        BooleanBuilder predicate = prepareBaseCondition(currentUser)
                .and(QUserReview.userReview.product.barcode.code.equalsIgnoreCase(barcode));
        return userReviewRepository.findOne(predicate).orElse(null);
    }

    private BooleanBuilder prepareBaseCondition(User user) {
        BooleanBuilder b = new BooleanBuilder();
        b.and(QUserReview.userReview.active.eq(true));
        b.and(QUserReview.userReview.deleted.eq(false));
        b.and(QUserReview.userReview.user.email.equalsIgnoreCase(user.getEmail()));
        return b;
    }

    private Predicate prepareFilterPredicate(UserReviewFilterDto filter) {
        BooleanBuilder b = new BooleanBuilder();

        if (filter.getFavourite() != null) {
            b.and(QUserReview.userReview.favourite.eq(filter.getFavourite()));
        }
        if (!StringUtils.isEmpty(filter.getName())) {
            b.andAnyOf(
                    QUserReview.userReview.product.name.containsIgnoreCase(filter.getName()),
                    QUserReview.userReview.product.manufacturer.name.containsIgnoreCase(filter.getName()),
                    QUserReview.userReview.product.barcode.code.equalsIgnoreCase(filter.getName())
            );
        }

        if (filter.getCategoryId() != null) {
            b.and(QUserReview.userReview.product.category.id.eq(filter.getCategoryId()));
        }

        return b;
    }
}
