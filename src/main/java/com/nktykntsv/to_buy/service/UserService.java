package com.nktykntsv.to_buy.service;

import com.nktykntsv.to_buy.exception.ItemNotFoundException;
import com.nktykntsv.to_buy.jpa.domain.User;
import com.nktykntsv.to_buy.jpa.repository.UserRepository;
import com.nktykntsv.to_buy.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new ItemNotFoundException(String.format("User [%s] was not found", email)));
    }

    public User getCurrentUser() {
        return this.getUserByEmail(SecurityUtils.getCurrentUserEmail());
    }
}
