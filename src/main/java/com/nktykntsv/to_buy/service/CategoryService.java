package com.nktykntsv.to_buy.service;

import com.nktykntsv.to_buy.exception.BusinessLogicException;
import com.nktykntsv.to_buy.exception.ItemNotFoundException;
import com.nktykntsv.to_buy.jpa.domain.Category;
import com.nktykntsv.to_buy.jpa.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.Null;
import java.util.List;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category findById(Long categoryId) {
        return categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ItemNotFoundException(String.format("Category with ID [%d] was not found", categoryId)));
    }

    public List<Category> getAllChildCategories(@Null Category parentCategory) {
        return parentCategory == null
                ? categoryRepository.findAllByParentCategoryIsNullOrderByNameAsc()
                : categoryRepository.findAllByParentCategoryOrderByNameAsc(parentCategory);
    }

    public List<Category> getAllChildCategories() {
        return categoryRepository.findAllByParentCategoryIsNotNull();
    }

    @Transactional
    public Category resolveCategory(Category category) {
        if (category.getId() == null) {
            throw new BusinessLogicException("Категория должна быть выбрана");
        }
        Category existingCategory = this.findById(category.getId());
        if (existingCategory.getParentCategory() == null) {
            throw new BusinessLogicException("Нельзя выбрать корневую категорию");
        }
        return existingCategory;
    }
}
