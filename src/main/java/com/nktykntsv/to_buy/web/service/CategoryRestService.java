package com.nktykntsv.to_buy.web.service;

import com.nktykntsv.to_buy.jpa.domain.Category;
import com.nktykntsv.to_buy.service.CategoryService;
import com.nktykntsv.to_buy.web.dto.CategoryDto;
import com.nktykntsv.to_buy.web.dto.IdNameDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryRestService {

    private final CategoryService categoryService;
    private final ModelMapper modelMapper;

    @Autowired
    public CategoryRestService(CategoryService categoryService, ModelMapper modelMapper) {
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
    }

    public List<IdNameDto> getRootCategories() {
        return categoryService.getAllChildCategories(null).stream()
                .map(o -> modelMapper.map(o, IdNameDto.class))
                .collect(Collectors.toList());
    }

    public CategoryDto getCategoryById(Long categoryId) {
        Category currentCategory = categoryService.findById(categoryId);
        List<IdNameDto> childCategories = categoryService.getAllChildCategories(currentCategory).stream()
                .map(o -> modelMapper.map(o, IdNameDto.class))
                .collect(Collectors.toList());
        CategoryDto currentCategoryDto = modelMapper.map(currentCategory, CategoryDto.class);
        currentCategoryDto.setChildCategories(childCategories);

        return currentCategoryDto;
    }

    public List<CategoryDto> getChildCategories() {
        return categoryService.getAllChildCategories().stream()
                .map(o -> modelMapper.map(o, CategoryDto.class))
                .collect(Collectors.toList());
    }
}
