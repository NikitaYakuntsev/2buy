package com.nktykntsv.to_buy.web.service;

import com.nktykntsv.to_buy.jpa.domain.BaseEntity;
import com.nktykntsv.to_buy.jpa.repository.BarcodeTypeRepository;
import com.nktykntsv.to_buy.jpa.repository.ManufacturerRepository;
import com.nktykntsv.to_buy.jpa.repository.TagRepository;
import com.nktykntsv.to_buy.web.dto.IdNameDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MiscRestService {

    private final TagRepository tagRepository;
    private final ModelMapper modelMapper;
    private final BarcodeTypeRepository barcodeTypeRepository;
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public MiscRestService(TagRepository tagRepository, ModelMapper modelMapper, BarcodeTypeRepository barcodeTypeRepository, ManufacturerRepository manufacturerRepository) {
        this.tagRepository = tagRepository;
        this.modelMapper = modelMapper;
        this.barcodeTypeRepository = barcodeTypeRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    public List<IdNameDto> getAllTags() {
        return prepareDtoList(tagRepository.findAll());
    }

    public List<IdNameDto> getAllBarcodeTypes() {
        return prepareDtoList(barcodeTypeRepository.findAll());
    }

    public List<IdNameDto> getAllManufacturers() {
        return prepareDtoList(manufacturerRepository.findAll());
    }

    private List<IdNameDto> prepareDtoList(List<? extends BaseEntity> enitites) {
        return enitites.stream()
                .map(entity -> modelMapper.map(entity, IdNameDto.class))
                .sorted(Comparator.comparing(IdNameDto::getName))
                .collect(Collectors.toList());
    }
}
