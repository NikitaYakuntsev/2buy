package com.nktykntsv.to_buy.web.service;

import com.nktykntsv.to_buy.jpa.domain.Product;
import com.nktykntsv.to_buy.jpa.domain.User;
import com.nktykntsv.to_buy.jpa.domain.UserReview;
import com.nktykntsv.to_buy.service.ProductService;
import com.nktykntsv.to_buy.service.UserReviewService;
import com.nktykntsv.to_buy.service.UserService;
import com.nktykntsv.to_buy.web.dto.ProductDto;
import com.nktykntsv.to_buy.web.dto.ProductReviewDto;
import com.nktykntsv.to_buy.web.dto.ReviewDto;
import com.nktykntsv.to_buy.web.dto.UserReviewFilterDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserReviewRestService {

    private final UserService userService;
    private final UserReviewService userReviewService;
    private final ProductService productService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserReviewRestService(UserService userService, UserReviewService userReviewService, ProductService productService, ModelMapper modelMapper) {
        this.userService = userService;
        this.userReviewService = userReviewService;
        this.productService = productService;
        this.modelMapper = modelMapper;
    }

    public ProductReviewDto createNewProductReview(ProductReviewDto dto) {
        Product product = modelMapper.map(dto.getProduct(), Product.class);
        UserReview review = modelMapper.map(dto.getReview(), UserReview.class);
        User user = userService.getCurrentUser();
        UserReview createdReview = userReviewService.addUserReview(user, product, review.getRating(), review.getComment(), review.getFavourite());
        Product createdProduct = createdReview.getProduct();
        return prepareProductReviewDto(createdProduct, createdReview);
    }

    public Page<ProductReviewDto> getUserReviews(UserReviewFilterDto filter, Pageable pageable) {
        Page<UserReview> userReviews = userReviewService.getUserReviews(userService.getCurrentUser(), filter, pageable);
        List<ProductReviewDto> productReviewDtos = userReviews.getContent().stream()
                .map(review -> {
                    ProductDto productDto = modelMapper.map(review.getProduct(), ProductDto.class);
                    ReviewDto reviewDto = modelMapper.map(review, ReviewDto.class);
                    return new ProductReviewDto(productDto, reviewDto);
                })
                .collect(Collectors.toList());
        return new PageImpl<>(productReviewDtos, pageable, userReviews.getTotalElements());
    }

    public ProductReviewDto getUserReviewByBarcode(String barcode) {
        UserReview userReview = userReviewService.getUserReviewByBarcode(userService.getCurrentUser(), barcode);
        Product product = userReview != null ? userReview.getProduct() : productService.findProductByBarcode(barcode);
        return prepareProductReviewDto(product, userReview);
    }

    private ProductReviewDto prepareProductReviewDto(Product product, UserReview userReview) {
        return new ProductReviewDto(
                product == null ? null : modelMapper.map(product, ProductDto.class),
                userReview == null ? null : modelMapper.map(userReview, ReviewDto.class)
        );
    }

}
