package com.nktykntsv.to_buy.web.service;

import com.nktykntsv.to_buy.jpa.domain.Product;
import com.nktykntsv.to_buy.jpa.domain.UserReview;
import com.nktykntsv.to_buy.service.ProductService;
import com.nktykntsv.to_buy.service.UserReviewService;
import com.nktykntsv.to_buy.service.UserService;
import com.nktykntsv.to_buy.web.dto.ProductDto;
import com.nktykntsv.to_buy.web.dto.ProductReviewDto;
import com.nktykntsv.to_buy.web.dto.ReviewDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductRestService {

    private final ProductService productService;
    private final UserReviewService userReviewService;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public ProductRestService(ProductService productService, UserReviewService userReviewService, UserService userService, ModelMapper modelMapper) {
        this.productService = productService;
        this.userReviewService = userReviewService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    public ProductReviewDto findProductById(Long id) {
        Product product = productService.findProductById(id);
        UserReview userReview = userReviewService.getUserReviewByProduct(userService.getCurrentUser(), product);
        return new ProductReviewDto(
                modelMapper.map(product, ProductDto.class),
                userReview == null ? null : modelMapper.map(userReview, ReviewDto.class)
        );
    }
}
