package com.nktykntsv.to_buy.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductReviewDto {
    private ProductDto product;
    private ReviewDto review;
}
