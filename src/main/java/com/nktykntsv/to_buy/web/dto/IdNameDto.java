package com.nktykntsv.to_buy.web.dto;

import lombok.Data;

@Data
public class IdNameDto {
    protected Long id;
    protected String name;
}
