package com.nktykntsv.to_buy.web.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.ZonedDateTime;

@Data
public class ReviewDto {
    @NotEmpty
    private String comment;
    @Min(value = 1)
    @Max(value = 5)
    private Integer rating;
    private Boolean favourite;
    private ZonedDateTime reviewDate;
}
