package com.nktykntsv.to_buy.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductDto extends IdNameDto {
    @NotNull
    private CategoryDto category;
    @NotNull
    private IdNameDto manufacturer;
    private BarcodeDto barcode;
    @JsonIgnore
    private List<IdNameDto> userTags;
    @JsonIgnore
    private List<Long> imageIds;
}
