package com.nktykntsv.to_buy.web.dto;

import lombok.Data;

@Data
public class UserReviewFilterDto {
    private Boolean favourite;
    private String name;
    private Long categoryId;
}
