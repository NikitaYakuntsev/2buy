package com.nktykntsv.to_buy.web.dto;

import lombok.Data;

import java.util.List;

@Data
public class CategoryDto extends IdNameDto {
    private IdNameDto parentCategory;
    private List<IdNameDto> childCategories;
}
