package com.nktykntsv.to_buy.web.helper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto {
    private String errorMessage;
    private String code;

    public ErrorDto(String errorMessage, @NotNull HttpStatus httpStatus) {
        this.errorMessage = errorMessage;
        this.code = String.valueOf(httpStatus.value());
    }
}