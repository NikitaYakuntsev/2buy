package com.nktykntsv.to_buy.web.helper;

import com.nktykntsv.to_buy.exception.BusinessLogicException;
import com.nktykntsv.to_buy.exception.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ExceptionHandlingController {

    @Value("${app.email}")
    private String contactEmail;

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ErrorDto handleRuntimeException(RuntimeException e) {
        log.error("Handle RuntimeException: {}", e.getMessage(), e);
//        return new ErrorDto("Internal Server Error. Please contact administrator " + contactEmail,
//                HttpStatus.INTERNAL_SERVER_ERROR);
        return new ErrorDto(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ItemNotFoundException.class)
    @ResponseBody
    public ErrorDto handleItemNotFoundException(ItemNotFoundException e) {
        return new ErrorDto(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(BusinessLogicException.class)
    @ResponseBody
    public ErrorDto handleBusinessLogicException(BusinessLogicException e) {
        return new ErrorDto(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
    }

}
