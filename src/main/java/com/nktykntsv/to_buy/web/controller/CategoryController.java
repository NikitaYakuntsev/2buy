package com.nktykntsv.to_buy.web.controller;

import com.nktykntsv.to_buy.web.service.CategoryRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    private final CategoryRestService categoryRestService;

    @Autowired
    public CategoryController(CategoryRestService categoryRestService) {
        this.categoryRestService = categoryRestService;
    }

    @GetMapping("/")
    public ResponseEntity<?> getRootCategories() {
        return ResponseEntity.ok(categoryRestService.getRootCategories());
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<?> getCategoryById(@PathVariable(value = "categoryId") Long categoryId) {
        return ResponseEntity.ok(categoryRestService.getCategoryById(categoryId));
    }
}
