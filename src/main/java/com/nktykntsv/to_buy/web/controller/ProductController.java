package com.nktykntsv.to_buy.web.controller;

import com.nktykntsv.to_buy.web.service.ProductRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductRestService productRestService;

    @Autowired
    public ProductController(ProductRestService productRestService) {
        this.productRestService = productRestService;
    }

    @GetMapping("/{productId}")
    public ResponseEntity<?> getProductById(@PathVariable("productId") Long productId) {
        return ResponseEntity.ok(productRestService.findProductById(productId));
    }

}
