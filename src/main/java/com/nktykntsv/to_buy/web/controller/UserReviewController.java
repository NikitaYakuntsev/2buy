package com.nktykntsv.to_buy.web.controller;

import com.nktykntsv.to_buy.web.dto.ProductReviewDto;
import com.nktykntsv.to_buy.web.dto.UserReviewFilterDto;
import com.nktykntsv.to_buy.web.helper.OffsetBasedPageRequest;
import com.nktykntsv.to_buy.web.service.UserReviewRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/review")
public class UserReviewController {

    private final UserReviewRestService userReviewRestService;

    @Autowired
    public UserReviewController(UserReviewRestService userReviewRestService) {
        this.userReviewRestService = userReviewRestService;
    }

    @GetMapping
    public ResponseEntity<?> getAllReviews(UserReviewFilterDto filter, Integer offset, Integer limit) {
        return ResponseEntity.ok(userReviewRestService.getUserReviews(filter, new OffsetBasedPageRequest(limit, offset)));
    }

    @GetMapping("/{barcode}")
    public ResponseEntity<?> getReviewByBarcode(@PathVariable("barcode") String barcode) {
        return ResponseEntity.ok(userReviewRestService.getUserReviewByBarcode(barcode));
    }

    @PostMapping
    public ResponseEntity<?> submitProductReview(@RequestBody ProductReviewDto reviewDto) {
        return ResponseEntity.ok(userReviewRestService.createNewProductReview(reviewDto));
    }
}
