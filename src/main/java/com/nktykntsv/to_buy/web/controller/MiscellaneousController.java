package com.nktykntsv.to_buy.web.controller;

import com.nktykntsv.to_buy.web.service.CategoryRestService;
import com.nktykntsv.to_buy.web.service.MiscRestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/misc")
@RequiredArgsConstructor
public class MiscellaneousController {

    private final MiscRestService miscRestService;
    private final CategoryRestService categoryRestService;

    @GetMapping("/barcode-type")
    public ResponseEntity<?> getAllBarcodeTypes() {
        return ResponseEntity.ok(miscRestService.getAllBarcodeTypes());
    }

    @GetMapping("/manufacturer")
    public ResponseEntity<?> getAllManufacturers() {
        return ResponseEntity.ok(miscRestService.getAllManufacturers());
    }

    @GetMapping("/category")
    public ResponseEntity<?> getAllAssignableCategories() {
        return ResponseEntity.ok(categoryRestService.getChildCategories());
    }

    @GetMapping("/tag")
    public ResponseEntity<?> getAllTags() {
        return ResponseEntity.ok(miscRestService.getAllTags());
    }


}
