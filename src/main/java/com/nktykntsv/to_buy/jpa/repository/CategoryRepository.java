package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    // FIXME: 25.04.2021 sort by Order field
    List<Category> findAllByParentCategoryOrderByNameAsc(Category parentCategory);
    List<Category> findAllByParentCategoryIsNullOrderByNameAsc();

    List<Category> findAllByParentCategoryIsNotNull();
}
