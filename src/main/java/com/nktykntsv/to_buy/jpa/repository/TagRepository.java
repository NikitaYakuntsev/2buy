package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
