package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findTopByBarcode_Code(String barcode);
}
