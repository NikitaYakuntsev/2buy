package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.BarcodeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BarcodeTypeRepository extends JpaRepository<BarcodeType, Long> {
}
