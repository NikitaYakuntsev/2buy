package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.UserReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

public interface UserReviewRepository extends JpaRepository<UserReview, Long>, QuerydslPredicateExecutor<UserReview> {

    @Modifying
    @Query("update UserReview r " +
            "set r.active = false " +
            "where r.user.id = :userId and r.product.id = :productId")
    void deactivatePreviousReviews(@Param("userId") Long userId, Long productId);
}
