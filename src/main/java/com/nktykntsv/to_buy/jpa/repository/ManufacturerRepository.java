package com.nktykntsv.to_buy.jpa.repository;

import com.nktykntsv.to_buy.jpa.domain.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
}
