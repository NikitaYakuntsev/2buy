package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "barcodetype", schema = "to_buy")
@Getter
@Setter
public class BarcodeType extends BaseEntity {
    @Column(name = "name")
    private String name;
}
