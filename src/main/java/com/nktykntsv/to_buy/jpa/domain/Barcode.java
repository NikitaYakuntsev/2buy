package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "barcode", schema = "to_buy")
@Getter
@Setter
public class Barcode extends BaseEntity {
    @Column(name = "code")
    private String code;

    @Column(name = "barcodetypeid")
    private Long barcodeType = 2L;
}
