package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user", schema = "to_buy")
@Getter
@Setter
public class User extends BaseEntity {
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "registrationdate")
    private String registrationDate;
    @Column(name = "passwordhash")
    private String passwordHash;
    @Column(name = "salt")
    private String salt;


}
