package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "userreview", schema = "to_buy")
@Getter
@Setter
public class UserReview extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "productid")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

    @Column(name = "reviewdate")
    private ZonedDateTime reviewDate;
    @Column(name = "rating")
    private Integer rating;
    @Column(name = "comment")
    private String comment;
    @Column(name = "favourite")
    private Boolean favourite;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "deleted")
    private Boolean deleted;

}
