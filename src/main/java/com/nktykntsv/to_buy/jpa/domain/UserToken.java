package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "usertoken", schema = "to_buy")
@Getter
@Setter
public class UserToken extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

    @Column(name = "value")
    private String value;
    @Column(name = "issuedate")
    private ZonedDateTime issueDate;
    @Column(name = "expectedexpirationdate")
    private ZonedDateTime expectedExpirationDate;
    @Column(name = "expirationdate")
    private ZonedDateTime expirationDate;
}
