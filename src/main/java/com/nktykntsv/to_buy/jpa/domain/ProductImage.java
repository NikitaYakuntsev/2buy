package com.nktykntsv.to_buy.jpa.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "productimage", schema = "to_buy")
@Getter
@Setter
public class ProductImage extends BaseEntity {
    @Column(name = "name")
    private String name;
    @Column(name = "content")
    private byte[] content;

    @ManyToOne
    @JoinColumn(name = "productid")
    private Product product;
}
