alter table to_buy.userreview
    ALTER COLUMN favourite DROP DEFAULT,
    alter column favourite type BOOLEAN using CASE WHEN favourite = 0 THEN FALSE WHEN favourite = 1 THEN TRUE END,
    alter column favourite set not null,
    alter column favourite set default FALSE;

alter table to_buy.userreview
    ALTER COLUMN active DROP DEFAULT,
    alter column active type BOOLEAN using CASE WHEN active = 0 THEN FALSE WHEN active = 1 THEN TRUE END,
    alter column active set default FALSE;

alter table to_buy.userreview
    ALTER COLUMN deleted DROP DEFAULT,
    alter column deleted type BOOLEAN using CASE WHEN deleted = 0 THEN FALSE WHEN deleted = 1 THEN TRUE END,
    alter column deleted set default FALSE;

