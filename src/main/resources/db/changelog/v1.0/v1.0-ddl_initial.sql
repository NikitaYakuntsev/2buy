DROP SCHEMA IF EXISTS to_buy;

CREATE SCHEMA IF NOT EXISTS to_buy;

CREATE TABLE IF NOT EXISTS to_buy.Category
(
    ID               SERIAL,
    Name             VARCHAR(255) NOT NULL,
    ParentCategoryID INT          NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_ProductCategory_ProductCategory_ID
        FOREIGN KEY (ParentCategoryID)
            REFERENCES to_buy.Category (ID)

)
;

CREATE INDEX FK_Category_ProductCategory_ID_idx ON to_buy.Category (ParentCategoryID);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ufacturer`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.Manufacturer
(
    ID   SERIAL,
    Name VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID)
)
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** codeType`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.BarcodeType
(
    ID   SERIAL,
    Name VARCHAR(255) NULL,
    PRIMARY KEY (ID)
)
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** code`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.Barcode
(
    ID            SERIAL,
    Code          VARCHAR(255) NULL,
    BarcodeTypeID INT          NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_BarcodeType_BarcodeTypeID
        FOREIGN KEY (BarcodeTypeID)
            REFERENCES to_buy.BarcodeType (ID)

)
;

CREATE INDEX FK_Barcode_BarcodeTypeID_idx ON to_buy.Barcode (BarcodeTypeID);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** duct`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.Product
(
    ID             SERIAL,
    Name           VARCHAR(255) NULL,
    CategoryID     INT          NOT NULL,
    ManufacturerID INT          NOT NULL,
    BarcodeID      INT          NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_Category_CategoryID
        FOREIGN KEY (CategoryID)
            REFERENCES to_buy.Category (ID),
    CONSTRAINT FK_Manufacturer_ManufacturerID
        FOREIGN KEY (ManufacturerID)
            REFERENCES to_buy.Manufacturer (ID),
    CONSTRAINT FK_Barcode_BarcodeID
        FOREIGN KEY (BarcodeID)
            REFERENCES to_buy.Barcode (ID)

)
;

CREATE INDEX FK_Product_CategoryID_idx ON to_buy.Product (CategoryID);
CREATE INDEX FK_Product_ManufacturerID_idx ON to_buy.Product (ManufacturerID);
CREATE INDEX FK_Product_BarcodeID_idx ON to_buy.Product (BarcodeID);


-- SQLINES DEMO *** ------------------------------------
-- Table `to_buy`.`User`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.User
(
    ID               SERIAL,
    FirstName        VARCHAR(255) NULL,
    LastName         VARCHAR(255) NULL,
    Email            VARCHAR(255) NOT NULL,
    Mobile           VARCHAR(255) NULL,
    RegistrationDate VARCHAR(255) NULL,
    PasswordHash     VARCHAR(255) NOT NULL,
    Salt             VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT Email_UNIQUE UNIQUE (Email),
    CONSTRAINT Mobile_UNIQUE UNIQUE (Mobile)
)
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** rReview`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.UserReview
(
    ID         SERIAL,
    ProductID  INT           NOT NULL,
    UserID     INT           NOT NULL,
    ReviewDate TIMESTAMP(0)  NOT NULL,
    Rating     INT           NOT NULL,
    Comment    VARCHAR(1500) NOT NULL,
    Favourite  SMALLINT      NULL,
    Active     SMALLINT      NOT NULL DEFAULT 1,
    Deleted    SMALLINT      NOT NULL DEFAULT 0,
    PRIMARY KEY (ID),
    CONSTRAINT FK_Product_ProductID
        FOREIGN KEY (ProductID)
            REFERENCES to_buy.Product (ID),
    CONSTRAINT FK_User_UserID
        FOREIGN KEY (UserID)
            REFERENCES to_buy.User (ID)

)
;

CREATE INDEX FK_UserReview_ProductID_idx ON to_buy.UserReview (ProductID);
CREATE INDEX FK_UserReview_UserID_idx ON to_buy.UserReview (UserID);


-- SQLINES DEMO *** ------------------------------------
-- Table `to_buy`.`Tag`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.Tag
(
    ID   SERIAL,
    Name VARCHAR(255) NULL,
    PRIMARY KEY (ID)
)
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** rTag`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.UserTag
(
    ID        SERIAL,
    TagID     INT NOT NULL,
    ProductID INT NOT NULL,
    UserID    INT NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT UQ_TagID_UserID_ProductID UNIQUE (TagID, ProductID, UserID),
    CONSTRAINT FK_Tag_TagID
        FOREIGN KEY (TagID)
            REFERENCES to_buy.Tag (ID),
    CONSTRAINT FK_Product_ProductID
        FOREIGN KEY (ProductID)
            REFERENCES to_buy.Product (ID),
    CONSTRAINT FK_User_UserID
        FOREIGN KEY (UserID)
            REFERENCES to_buy.User (ID)

)
;

CREATE INDEX FK_UserTag_TagID_idx ON to_buy.UserTag (TagID);
CREATE INDEX FK_UserTag_ProductID_idx ON to_buy.UserTag (ProductID);
CREATE INDEX FK_UserTag_UserID_idx ON to_buy.UserTag (UserID);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** rToken`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.UserToken
(
    ID                     SERIAL,
    Value                  VARCHAR(255) NOT NULL,
    IssueDate              TIMESTAMP(0) NOT NULL,
    ExpectedExpirationDate TIMESTAMP(0) NOT NULL,
    ExpirationDate         TIMESTAMP(0) NULL,
    UserID                 INT          NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_User_UserID
        FOREIGN KEY (UserID)
            REFERENCES to_buy.User (ID)

)
;

CREATE INDEX FK_UserToken_UserID_idx ON to_buy.UserToken (UserID);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ductImage`
-- SQLINES DEMO *** ------------------------------------
CREATE TABLE IF NOT EXISTS to_buy.ProductImage
(
    ID        SERIAL,
    Name      VARCHAR(255) NULL,
    Content   BYTEA        NULL,
    ProductID INT          NOT NULL,
    PRIMARY KEY (ID),
    CONSTRAINT FK_Product_ProductID
        FOREIGN KEY (ProductID)
            REFERENCES to_buy.Product (ID)

)
;

CREATE INDEX FK_ProductImage_ProductID_idx ON to_buy.ProductImage (ProductID);


/* SET SQL_MODE=@OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; */
