FROM openjdk:11.0.1-jdk-oraclelinux7

MAINTAINER Nikita Yakuntsev <nikita@yakuntsev.ru>

COPY ./target/2buy-*.jar /app/app.jar
CMD ["java", "-Xmx1024M", "-Xms1024M", "-server", "-XX:+CMSClassUnloadingEnabled", "-Dspring.profiles.active=prod", "-jar", "/app/app.jar"]
EXPOSE 8080